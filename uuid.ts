

  // Java UUID -> JS
  export function uuidToString(uuid) {
    var uuidStr = uuid.toString();
    var pattern = java.util.regex.Pattern.compile("0000(.{4})-0000-1000-8000-00805f9b34fb", 2);
    var matcher = pattern.matcher(uuidStr);
    return matcher.matches() ? matcher.group(1) : uuidStr;
  };

  // JS UUID -> Java
 export function stringToUuid(uuidStr:string|string[]) {
    if (uuidStr.length === 4) {
      uuidStr = "0000" + uuidStr + "-0000-1000-8000-00805f9b34fb";
    }
    return java.util.UUID.fromString(uuidStr as string);
  };