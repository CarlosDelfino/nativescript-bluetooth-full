
import * as observableModule from "data/observable";
import * as observableArrayModule from "data/observable-array";
import { BluetoothStatus } from "./bluetooth-common";

export * from "./bluetooth-common"

export class BluetoothInputStream {

  available(): number;

  markSupported(): boolean;

  mark(pos: number): void;
  /**
			 * Reads a single byte from this stream and returns it as an integer in the range from 0 to 255. Returns -1 if the end of the stream has been reached. Blocks until one byte has been read, the end of the source stream is detected or an exception is thrown.
			 */
  read(): number;
  /**
   * Equivalent to read(buffer, 0, buffer.length).
   */
  read(buffer: native.Array<number>): number;
  /**
   * Reads at most length bytes from this stream and stores them in the byte array b starting at offset.
   * @param buffer 
   * @param offset 
   * @param length 
   */
  read(buffer: native.Array<number>, offset: number, length: number): number;
  /**
   * Resets this stream to the last marked location. Throws an IOException if the number of bytes read since the mark has been set is greater than the limit provided to mark, or if no mark has been set.
   */
}


/**
 * The options object passed into the startScanning function.
 */
export class StartScanningOptions {
  /**
   * Zero or more services which the peripheral needs to broadcast.
   * Default: [], which matches any peripheral.
   */
  serviceUUIDs?: string[];

  /**
   * The number of seconds to scan for services.
   * Default: unlimited, which is not really recommended. You should stop scanning manually by calling 'stopScanning'.
   */
  seconds?: number;

  /**
   * This callback is invoked when a peripheral is found.
   */
  onDiscovered: (data: Peripheral) => void;
}   

export class ECGSignal{
  level: number;
  /** 
   * tempo em ms de coleta com bom sinal 
   * 
   */
  timeRun: number;

  /**
   * Tempo de espera por bom sinal
   */
  timeOut: number;
}

/**
 * The options object passed into the connect function.
 */
export class ConnectOptions extends Peripheral {

  /**
   * Qualidade de sinal mínima para considerar um bom sinal.
   * Neste equipamento (shishiray) só existe dois níveis 0 ou 200, sendo 200 o bom sinal.
   */
  signal?: ECGSignal;
  /**
   * Tempo que o processo de obtenção de dados adormece
   */
  sleepTimerRead?: number;

  timeOutConnect?: number;

  waitConnect?;

  tryConnect?;

  limitOfPayloads?: number;

  maxTimeGetData?: number;

  /**
   * 
   */
  bufferSize?: number;
  /** 
   * A conexão deve ser por Bluetoot LE
   */
  useLE?: boolean;

  service?: Service;

  /**
   * Lista de UUIDs a serem obtidos
   */
  fetchUUIDS?: string[];
}

/**
 * The returned object in several callback functions.
 */
export class Peripheral {

  /**
   * Once the peripheral is connected this callback function is invoked.
   */
  onConnecting?: (data: Peripheral) => void;
  onConnected?: (data: Peripheral) => void;

  /**
   * Once the peripheral is disconnected this callback function is invoked.
   */
  onDisconnecting?: (data: Peripheral) => void;
  onDisconnected?: (data: Peripheral) => void;

  type?: string; // por exemplo se é RFCOMM

  state?: BluetoothStatus;

  /**
   * The UUID of the peripheral.
   */
  UUID?: string;

  /**
   * A friendly description of the peripheral as provided by the manufacturer.
   */
  name?: string;

  // state: string; // TODO not sure we'll keep this, so not adding it here for now
  /**
   * The relative signal strength which more or less can be used to determine how far away the peripheral is.
   */
  RSSI?: number;
  /**
   * Once connected to the peripheral a list of services will be set.
   */
  services?: Service[];

  dataProcessor?: BluetoothDataProcessor;

  device?: any;

  /*
   * Tipicamente usado pelo RFCOMM
   */
  // se tipo for rfcom por exemplo define se a conexão deve ser segura ou não
  secure?: boolean; // se deve usar canal seguro


  // indica o indice a contar do zero referente ao serviço usado para transmissão de dados, normalmente se refere ao RFCOMM;
  index_service_data?: number;

}

/**
 * A service provided by a periperhal.
 */
export class Service {
  /**
   * The UUID of the service.
   */
  UUID: string;
  /**
   * Depending on the peripheral and platform this may be a more friendly description of the service.
   */
  name?: string;
  /**
   * A list of service characteristics a client can interact with by reading, writing, subscribing, etc.
   */
  characteristics?: Characteristic[];

  /**
   * indica se o serviço usa segurança ou não, como no caso do rfcomm se é um canal seguro
   */
  secure: boolean
}

export interface BluetoothDataProcessor {
  stop();
  start();

}

/**
 * A characteristic provided by a service.
 */
export class Characteristic {
  /**
   * The UUID of the characteristic.
   */
  UUID: string;
  /**
   * Depending on the service and platform (iOS only) this may be a more friendly description of the characteristic.
   * On Android it's always the same as the UUID.
   */
  name: string;
  /**
   * An object containing characteristic properties like read, write and notify.
   */
  properties: {
    read: boolean;
    write: boolean;
    writeWithoutResponse: boolean;
    notify: boolean;
    indicate: boolean;
    broadcast: boolean;
    authenticatedSignedWrites: boolean;
    extendedProperties: boolean;
  };

  /**
   * ignored for now
   */
  descriptors: any;

  /**
   * ignored for now
   */
  permissions: any;

}

/**
 * Response object for the read function
 */
export class ReadResult {
  value: any;
  valueRaw: any;
  characteristicUUID: string;
}


export class Bluetooth {

  private constructor();

  public static finalize(): void;

  public static isEnabled(): Promise<boolean>;

  public static initBluetoothDevice(options): Promise<any>;
  public static initBluetoothDevice(): Promise<any>;

  /**
   * Required for Android 6+ to be able to scan for peripherals in the background.
   */
  public static hasCoarseLocationPermission(): Promise<boolean>;

  /**
   * Required for Android 6+ to be able to scan for peripherals in the background.
   */
  public static requestCoarseLocationPermission(): Promise<any>;

  /**
   * Can be used to reduce the console.log messaging for characteristic read, write, change operations
   * @param enable Set to false to reduce console.log messages
   */
  public static setCharacteristicLogging(enable: boolean): void;

  public static startScanning(options: StartScanningOptions): Promise<any>;
  public static stopScanning(): Promise<any>;

  public static connect(options: ConnectOptions): Promise<any>;
  public static disconnect(options: ConnectOptions): Promise<void>;

  public static isDeviceConnected(options: ConnectOptions): boolean;

  public static getDataStream(options: ConnectOptions): BluetoothInputStream;

  public static getBondedDevices(): observableArrayModule.ObservableArray<Peripheral>;

  /**
   * Adiciona a lista de processadores de dados para um determinado dispositivo e serviços
   * identificado pelo parâmetro options que pode ser 
   * do tipo ConnectOptions, ReadOptions e WriteOptions
   */
  public static addDataProcessor(connectOptions: ConnectOptions | ReadOptions | WriteOptions, dataProcessor: BluetoothDataProcessor);
  public static removeDataProcessor(connectOptions: ConnectOptions | ReadOptions | WriteOptions);

  public static read(options: ReadOptions): Promise<ReadResult>;
  public static write(options: WriteOptions): Promise<any>;
  public static writeWithoutResponse(options: WriteOptions): Promise<any>;

  public static startNotifying(options: StartNotifyingOptions): Promise<any>;
  public static stopNotifying(options: StopNotifyingOptions): Promise<any>;
}

export class ReadOptions extends ConnectOptions {

}

export class WriteOptions extends ConnectOptions{

}

export class StartNotifyingOptions {

}

export class StopNotifyingOptions {

}