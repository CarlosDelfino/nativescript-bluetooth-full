export * from "./bluetooth-common"

import * as application from "application";
import { ad as androidUtils } from "utils/utils";

import { Observable } from "data/observable";
import { ObservableArray } from "data/observable-array";

import { ConnectOptions, WriteOptions, ReadOptions, BluetoothInputStream, Peripheral, Service, BluetoothStatus, BluetoothBase, BluetoothDataProcessor } from "./";

export class BluettoothInputStream {

  mark(pos: number): void {
    throw new Error("Method not implemented.");
  }

  private is: java.io.InputStream;
  constructor(is: java.io.InputStream) {
    this.is = is;
  }

  public markSupported(): boolean {
    return this.is.markSupported();
  }

  /**
   * Reads a single byte from this stream and returns it as an integer in the range from 0 to 255. Returns -1 if the end of the stream has been reached. Blocks until one byte has been read, the end of the source stream is detected or an exception is thrown.
          
   * Equivalent to read(buffer, 0, buffer.length).
          
   * Reads at most length bytes from this stream and stores them in the byte array b starting at offset.
   * @param buffer 
   * @param offset 
   * @param length
   */
  public read(buffer?: native.Array<number>, offset?: number, length?: number): number {
    if (buffer == undefined) return this.is.read();
    else if (offset != undefined && length != undefined) return this.is.read(buffer, offset, length);
    else return this.is.read(buffer);
  }

  public available(): number {
    return this.is.available();
  }

}

export class Bluetooth extends BluetoothBase {
  public static readonly PERMISSION_GRANTED = android.content.pm.PackageManager.PERMISSION_GRANTED;

  public static readonly ACCESS_COARSE_LOCATION_PERMISSION_REQUEST_CODE = 222;

  //public static const  ACCESS_COARSE_LOCATION = android.Manifest.permission.ACCESS_COARSE_LOCATION;
  //public static const  ACCESS_FINE_LOCATION = android.Manifest.permission.ACCESS_FINE_LOCATION;

  public static readonly A2DP = android.bluetooth.BluetoothProfile.A2DP;
  public static readonly EXTRA_PREVIOUS_STATE = android.bluetooth.BluetoothProfile.EXTRA_PREVIOUS_STATE;
  public static readonly EXTRA_STATE = android.bluetooth.BluetoothProfile.EXTRA_STATE;

  public static readonly HEADSET = android.bluetooth.BluetoothProfile.HEADSET;
  public static readonly HEALTH = android.bluetooth.BluetoothProfile.HEALTH;

  public static readonly STATE_CONNECTED = android.bluetooth.BluetoothProfile.STATE_CONNECTED;
  public static readonly STATE_CONNECTING = android.bluetooth.BluetoothProfile.STATE_CONNECTING;
  public static readonly STATE_DISCONNECTED = android.bluetooth.BluetoothProfile.STATE_DISCONNECTED;
  public static readonly STATE_DISCONNECTING = android.bluetooth.BluetoothProfile.STATE_DISCONNECTING;

  private static adapter;
  private static onDiscovered;
  private static le;

  /**
   * Connections are stored as key-val pairs of UUID-Connection.
   * So something like this:
   * [{
   *   34343-2434-5454: {
   *     state: 'connected',
   *     discoveredState: '',
   *     operationConnect: someCallbackFunction
   *   },
   *   1323213-21321323: {
   *     ..
   *   }
   * }, ..]
   */
  private static readonly _connections: Peripheral[] = new Array<Peripheral>();
  private static readonly _dataProcessors: BluetoothDataProcessor[] = new Array<BluetoothDataProcessor>();
  private static readonly _inDisconecionProcess: boolean[] = new Array<boolean>();

  private static _initBluetooth_LE_SDK21() {
    throw new Error("not implemented!");
  }

  private static _initBluetooth_LE_SDK18_a_21() {
    throw new Error("not implemented!");
  }

  public static initBluetoothDevice(btParam: ConnectOptions = {
    "useLE": true
  }) {
    console.log("BluetoothManager: Criando Promessa de Inicialização!");
    let promessa = new Promise((resolve, reject) => {
      // console.log("BluetoothManager: Parametros iniciais para obtenção do Bluetooth:");
      // console.dir(btParam);

      if (Bluetooth.adapter !== undefined) {
        console.log("BluetoothManager: adapter já obtido!");
        return resolve();
      }

      console.log("BluetoothManager: Obtendo adaptador do bluetooth!");
      /*
       * Tento obter o BluetoothAdapter.
       * nesta primeira versão para aceitar versões SDK inferiores a 18, e assim funcionar com o Bluetooth Classico, seguirei o proposto pelo manual do Android.
       * O restante da blioteca continuará usando o Bluetooth LE, no futuro vejo como agir neste caso.
       */
      if (btParam.useLE == false || android.os.Build.VERSION.SDK_INT < 18) {
        console.log("BluetoothAdapter: Usando Bluetooth Classico ou Android inferior ao 18 ou usando Bluetooth Classico!");
        Bluetooth.adapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
        Bluetooth.le = false;
        console.log("BluetoothManager: Adaptador do Bluettooth obtido!");
      }
      /**
        else if (android.os.Build.VERSION.SDK_INT >= 18) {
        console.log("BluetoothManager: Android superior ou igual a 18, e usando Bluetooth LE");
        var bluetoothManager;
        bluetoothManager = androidUtils.getApplicationContext().getSystemService(BLUETOOTH_SERVICE);
        adapter = bluetoothManager.getAdapter();
        le = true;
        console.log("BluetoothManager: Adaptador do Bluettooth LE obtido!");
      }
      **/

      if (Bluetooth.adapter === undefined)
        return reject("Não Foi possível obter o Adaptador Bluetooth!");
      console.log("BluetoothManager: Sucesso na obtenção do adapter!");

      if (btParam.useLE && android.os.Build.VERSION.SDK_INT >= 21 /*android.os.Build.VERSION_CODES.LOLLIPOP */) {
        console.log("BluetoothManager:  Usando BluetoothLE e Android superior ou igual a 21");
        Bluetooth._initBluetooth_LE_SDK21();
      } else if (!btParam.useLE && android.os.Build.VERSION.SDK_INT >= 17) /*android.os.Build.VERSION_CODES.JELLYBEAN */ {
        console.log("BluetoothManager: Não usar BluetoothLE e Android é superior ou igual a 17");
        //_scanCallback = { /* NÃO É NECESSÁRIO */ };
      } else if (android.os.Build.VERSION.SDK_INT >= 18) {
        console.log("BluetoothManager: Usar Bluetooth LE e android entre 18 e 21");
        Bluetooth._initBluetooth_LE_SDK18_a_21();
      } else {
        return reject("Não é possível usar Android SDK: " + android.os.Build.VERSION.SDK_INT);
      }
      console.log("BluetoothManager: initBluetooth finalizado com sucesso!");
      return resolve();
    });
    return promessa;
    // end return new Promise();
    // nada aqui será executado;
  };

  public static isEnabled() {
    return Bluetooth.adapter != undefined && Bluetooth.adapter.isEnabled();
  };




  public static getBondedDevices() {
    var obArrayDevices = new ObservableArray();
    // If there are paired devices
    const pairedDevices = Bluetooth.adapter.getBondedDevices().toArray();
    //  console.dir(pairedDevices);
    for (let i = 0; i < pairedDevices.length; i++) {
      let device = pairedDevices[i];
      let newDevice = {
        type: 'scanResult',
        UUID: device.getAddress(),
        name: device.getName(),
        RSSI: 0,
        state: 'disconnected' // seria interessante consultar se realmente está desconectado
      };
      obArrayDevices.push(newDevice);
    }
    return obArrayDevices;
  };

  private static _connectRFCOMM(connectOptions: ConnectOptions) {
    console.log("Bluetooth.Android._defaultOnConnectedRFCOMM: iniciando.");
    //  console.dir(connectOptions);

    const bluetoothDevice = Bluetooth.adapter.getRemoteDevice(connectOptions.UUID);

    if (connectOptions.fetchUUIDS && bluetoothDevice.fetchUuidsWithSdp()) {
      console.log("Bluetooth.Android._defaultOnConnectedRFCOMM: #########################################");
      console.log("Bluetooth.Android._defaultOnConnectedRFCOMM: fetch Uuids With Sdp");

      var uuids = bluetoothDevice.getUuids();
      for (var i = 0; i < uuids.length; i++) {
        console.log("Bluetooth.Android._defaultOnConnectedRFCOMM: " + uuids[i].getUuid());
      }
    }
    console.log("Bluetooth.Android._defaultOnConnectedRFCOMM: #########################################");

    let uuid = connectOptions.services[0].UUID;
    let sdp_UUID = java.util.UUID.fromString(uuid);
    console.log("Bluetooth.Android._defaultOnConnectedRFCOMM: SDP UUID: " + sdp_UUID);

    let service;
    if (connectOptions.services[0].secure) {
      //Obtem conexão segura
      console.log("Bluetooth.Android._defaultOnConnectedRFCOMM: Iniciando a conexão segura para dispositivos do tipo RFCOMM");
      service = bluetoothDevice.createRfcommSocketToServiceRecord(sdp_UUID);
    } else {
      //Obtem conexão comum
      console.log("Bluetooth.Android._defaultOnConnectedRFCOMM: Iniciando a conexão comum para dispositivos do tipo RFCOMM");
      service = bluetoothDevice.createInsecureRfcommSocketToServiceRecord(sdp_UUID);
    }

    if (service != null) {
      service.connect();
      console.log("Bluetooth.Android._defaultOnConnectedRFCOMM: " + service.isConnected());
    }
    return service;

  };

  public static addDataProcessor(connectOptions: ConnectOptions | ReadOptions | WriteOptions, dataProcessor: BluetoothDataProcessor) {
    Bluetooth._dataProcessors[connectOptions.UUID] = dataProcessor;
  }

  public static removeDataProcessor(connectOptions: ConnectOptions | ReadOptions | WriteOptions) {
    delete Bluetooth._dataProcessors[connectOptions.UUID];
  }

  // note that this doesn't make much sense without scanning first
  public static connect(connectOptions: ConnectOptions): Promise<Peripheral> {
    return new Promise(function (resolve, reject) {
      let deviceProfile = "GATT";
      try {
        // or macaddress..
        if (!connectOptions.UUID) {
          reject("No UUID was passed");
          return;
        }

        /*
              if(!adapter.checkBluetoothAddress(connectOptions.UUID)){
                reject("Endereço do Dispositivo inválido!");
                return;
              }
        */

        let bluetoothDevice = Bluetooth.adapter.getRemoteDevice(connectOptions.UUID);
        if (bluetoothDevice === null) {
          console.log("Bluetooth.Android: dispositivo não encontrado!");
          reject("Could not find peripheral with UUID " + connectOptions.UUID);
          return;
        } else {
          console.log("Bluetooth.Android: Connecting to peripheral with UUID: " + connectOptions.UUID);
          console.log("Bluetooth.Android: Connecting to peripheral with Name: " + bluetoothDevice.getName());

          let connectedData: Peripheral = {
            state: BluetoothStatus.disconnected,
            UUID: bluetoothDevice.getAddress(),
            name: bluetoothDevice.getName(),
            RSSI: 0,
            services: [{
              UUID: connectOptions.service.UUID,
              secure: connectOptions.service.secure,
            } as Service],
            dataProcessor: connectOptions.dataProcessor
          } as Peripheral;


          let deviceConnection;
          /*
           * Não vejo como possível criar um código para cada tipo de dispositivo existente,
           * o ideal é criar um mecanismo de plugins que podem ser anexados conforme o tipo
           * de dispositovo.
           * Sem falar que neste dispostivo em especial existem dois RFCOMM´s portanto
           * não é possível identificar todas estasw possibilidades e como se comportar em
           * tal situação
           */
          if (connectOptions.type && connectOptions.type === "RFCOMM") {
            deviceProfile = connectOptions.type;
            console.log("Bluetooth.Android: conexão do tipo RFCOMM");

            deviceConnection = Bluetooth._connectRFCOMM(connectedData);

            connectedData.state = deviceConnection != undefined && deviceConnection.isConnected() ? BluetoothStatus.connected : BluetoothStatus.disconnected;

          } else if (android.os.Build.VERSION.SDK_INT >= 18 && android.os.Build.VERSION.SDK_INT < 23 /*android.os.Build.VERSION_CODES.M */) {
            deviceConnection = bluetoothDevice.connectGatt(
              androidUtils.getApplicationContext(), // context
              false, // autoconnect
              new Bluetooth._MyGattCallback( /* TODO pass in onWhatever function */)
            );
          } else {
            deviceConnection = bluetoothDevice.connectGatt(
              androidUtils.getApplicationContext(), // context
              false, // autoconnect
              new Bluetooth._MyGattCallback( /* TODO pass in onWhatever function */),
              2 // android.bluetooth.BluetoothDevice.TRANSPORT_LE // 2
            );
          }

          if (deviceConnection == undefined) {
            return reject("Ouve algum erro na conexão e não se obteve a conexão do serviço!");
          }
          connectedData.type = deviceProfile;
          connectedData.device = deviceConnection; // agora pode ser tanto GATT como o próprio serviço se for do tipo RFCOMM
          console.log("Bluetooth.Android: Conexão pronta: ");
          //  console.dir(connectedData);

          try {
            if (connectOptions.onConnected) {
              console.log("Bluetooth.Android: usando calllback personalizado!");
              // precisa ser informado antes de chamar o callback.
              connectOptions.onConnected(connectedData);
            } else {
              console.log("Bluetooth.Android: Não foi informado um callback para tratar a conexão!");
            }
          } catch (ex) {
            return reject(ex);
          }

          Bluetooth._connections[connectOptions.UUID] = connectedData;
          return resolve(Bluetooth._connections[connectOptions.UUID]);
        }

      } catch (ex) {
        console.log("Bluetooth.connect.try.catch: Error " + ex);
        return reject(ex);
      }
    });
  };

  public static disconnect(connectOptions: ConnectOptions): Promise<void> {
    if (Bluetooth._inDisconecionProcess[connectOptions.UUID])
      throw new Error("Bluetooth em processo de desconexão, não pode ser chamado duas vezes seguida: " + connectOptions.UUID + " nome: " + connectOptions.name);

    Bluetooth._inDisconecionProcess[connectOptions.UUID] = true;
    return new Promise<void>(function (resolve, reject) {
      if (!connectOptions.UUID) {
        return reject("No UUID was passed");
      }
      const stateObject: Peripheral = Bluetooth._connections[connectOptions.UUID];
      if (!stateObject) {
        return reject("Peripheral wasn't connected");
      }
      Bluetooth._connections[connectOptions.UUID] = null;

      if (stateObject.onDisconnecting) {
        stateObject.onDisconnecting(stateObject);
      } else {
        console.log("----- !!! no disconnecting callback found");
      }
      Bluetooth._dataProcessors[connectOptions.UUID].stop();

      let device = stateObject.device;

      try {
        Bluetooth.close(stateObject);

        console.log("----- invoking disc cb");
        if (stateObject.onDisconnected) {
          stateObject.onDisconnected(stateObject);
        } else {
          console.log("----- !!! no disconnected callback found");
        }

        Bluetooth._inDisconecionProcess[connectOptions.UUID] = false;
        return resolve();
      } catch (ex) {
        console.log("Error in Bluetooth.disconnect: " + ex);

        Bluetooth._inDisconecionProcess[connectOptions.UUID] = false;
        return reject(ex);
      }
    });
  };
  private static close(p: Peripheral) {
    if (p.type == "RFCOMM") {
      console.log("BLUETOOTH.close()");
      // console.dir(p);
      p.device.close();
    }
  }
  public static deviceBondState(connectOptions) {
    if (!connectOptions.UUID) {
      throw "No UUID was passed";
    }
    let stateObject = Bluetooth._connections[connectOptions.UUID];
    if (!stateObject) {
      return "unknow";
    }

    let bs = "unknow";
    if (stateObject.type !== "RFCOMM")
      throw "Não implementado!!!";
    else {
      let socketDevice = stateObject.device;
      let remote = socketDevice.getRemoteDevice();
      let bsi = remote.getBondState();
      switch (bsi) {
        case 10:
          bs = "BOND NONE";
          break;
        case 11:
          bs = "BOND BONDING";
          break;
        case 12:
          bs = "BOND BONDED";
          break;
        default:
          bs = " Unknow!";
          break;
      }
      //console.log("bluetooth.android.js: Remote Device: " + bsi + " " + bs);
    }
    return bs;
  }
  public static isDeviceConnected(connectOptions) {
    if (!connectOptions.UUID) {
      throw "No UUID was passed";
    }
    let stateObject = Bluetooth._connections[connectOptions.UUID];
    if (!stateObject) {
      return false;
    }

    if (stateObject.type !== "RFCOMM")
      throw "Não implementado!!!";
    else {
      let socketDevice = stateObject.device;
      if (socketDevice != null) {
        return socketDevice.isConnected();
      } else return false;
    }
  };

  public static getDataStream(connectOptions: ConnectOptions): BluetoothInputStream {
    if (!connectOptions.UUID) {
      throw "No UUID was passed";
    }
    let stateObject = Bluetooth._connections[connectOptions.UUID];
    if (!stateObject) {
      throw "Dispositivo não foi conectado!";
    }

    if (stateObject.type !== "RFCOMM")
      throw "Não implementado!!!";
    else {
      let socketDevice = stateObject.device;
      if (socketDevice == null)
        throw "Verifique a conexão, algo deu errado pois não obteve o socket!";
      else if (!socketDevice.isConnected())
        throw "Verifique a conexão";
      else {
        let is = new BluettoothInputStream(socketDevice.getInputStream());
        return is;
      }
    }

  };

  public static finalize() {
    Bluetooth._connections.forEach(element => {

    });
  }
}